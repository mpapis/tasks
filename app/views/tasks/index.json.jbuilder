json.array!(@tasks) do |task|
  json.extract! task, :id, :name, :description, :deadline, :finished_at, :list_id
  json.url task_url(task, format: :json)
end
