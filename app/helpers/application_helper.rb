module ApplicationHelper
  # helper to make bootstrap3 nav-pill <li>'s with links in them, that have
  # proper 'active' class if active.
  #
  # the current pill will have 'active' tag on the <li>
  #
  # html_options param will apply to <li>, not <a>.
  #
  # can pass block which will be given to `link_to` as normal.
  def nav_to(label, link_params, html_options = {})
    current = current_page?(link_params)

    if current
      html_options[:class] ||= ""
      html_options[:class] << " active "
    end

    content_tag(:li, html_options) do
      link_to(label, link_params)
    end
  end
end
