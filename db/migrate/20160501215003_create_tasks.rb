class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :name
      t.text :description
      t.datetime :deadline
      t.datetime :finished_at
      t.references :list, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
